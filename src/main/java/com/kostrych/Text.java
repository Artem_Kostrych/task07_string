package com.kostrych;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Text {
    private List<Sentence> sentences;
    private Scanner in = new Scanner(System.in);

    public Text() {
        sentences = new ArrayList<>();
        String everything = readFile();
        divideBySentences(everything);

    }

    private String readFile() {
        String everything = null;
        try (BufferedReader br = new BufferedReader(new FileReader("file.txt"))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            everything = sb.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return everything;
    }

    private void divideBySentences(String everything) {
        Pattern p = Pattern.compile("[\\.?!]");
        Matcher m = p.matcher(everything);
        int start = 0;
        while (m.find()) {
            System.out.println(everything.substring(start, m.start() + 1).trim());
            sentences.add((new Sentence(everything.substring(start, m.start() + 1).trim())));
            start = m.end();
        }
    }

    //1
    public void findMaxAmount() {
        int maxAmount = 0;
        int currentAmount = 0;
        for (int i = 0; i < sentences.size(); i++) {
            for (int k = 0; k < sentences.get(i).getWords().size(); k++) {
                for (int j = i + 1; j < sentences.size(); j++) {
                    for (int m = 0; m < sentences.get(j).getWords().size(); m++) {
                        if (sentences.get(i).getWords().get(k).getWord().
                                equalsIgnoreCase(sentences.get(j).getWords().get(m).getWord())) {
                            currentAmount++;
                            break;
                        }
                    }
                }
                if (currentAmount > maxAmount) {
                    maxAmount = currentAmount;
                }
                currentAmount = 0;
            }
        }
        System.out.println("\nThe maximum number of identical words in sentence is: " + maxAmount);
    }

    //2
    public void outputSentencesInOrder() {
        System.out.println("\nSentence in ascending order.\n");
        for (int i = 1; i < Sentence.MAX_AMOUNT_WORDS; i++) {
            for (Sentence sentence : sentences) {
                if (sentence.getWords().size() == i) {
                    System.out.println(i + " " + sentence.getSentence());
                }
            }
        }
    }

    //3
    public void findWordInFirstSentence() {
        System.out.print("\nUnique word in first sentence: ");
        boolean isWord;
        for (int i = 0; i < sentences.get(0).getWords().size(); i++) {
            isWord = false;
            for (int j = 1; j < sentences.size(); j++) {
                for (int k = 0; k < sentences.get(j).getWords().size(); k++) {
                    if (sentences.get(0).getWords().get(i).getWord().
                            equalsIgnoreCase(sentences.get(j).getWords().get(k).getWord())) {
                        isWord = true;
                        break;
                    }
                }
            }
            if (isWord == false) {
                System.out.println(sentences.get(0).getWords().get(i).getWord());
                break;
            }
            if (i == sentences.get(0).getWords().size() - 1) {
                System.out.println("There is not.");
            }
        }
    }

    //4
    public void findWordsInQuestions() {
        System.out.println("Input length : ");
        Scanner in = new Scanner(System.in);
        int length = in.nextInt();
        Set<String> strings = new LinkedHashSet<>();
        for (int i = 0; i < sentences.size(); i++) {
            if (sentences.get(i).getSentence().charAt(sentences.get(i).getSentence().length() - 1) == '?') {
                for (Word word : sentences.get(i).getWords()) {
                    if (word.getWord().length() == length) {
                        strings.add(word.getWord());
                    }
                }
            }
        }
        if (strings.size() > 0) {
            System.out.println("Words in length " + length + " : " + strings);
        } else {
            System.out.println("Is not words which length is " + length);
        }
    }

    //5
    public void exchangeWords() {
        for (int i = 0; i < sentences.size(); i++) {
            String longestWord = new String();
            String startFromVowel = new String();
            for (Word word : sentences.get(i).getWords()) {
                if (startFromVowel.length() == 0 && startFromVowel(word.getWord()) == true) {
                    startFromVowel = word.getWord();
                }

                if (word.getWord().length() > longestWord.length()) {
                    longestWord = word.getWord();
                }
            }
            int startLongest = sentences.get(i).getSentence().indexOf(longestWord);
            int endLongest = startLongest + longestWord.length();
            int startVowel = sentences.get(i).getSentence().indexOf(startFromVowel);
            int endVowel = startVowel + startFromVowel.length();
            sentences.get(i).setSentence(sentences.
                    get(i).getSentence().replace(longestWord, startFromVowel));
            if (startLongest > startVowel) {
                sentences.get(i).setSentence(sentences.
                        get(i).getSentence().substring(0, startVowel) + longestWord + sentences.
                        get(i).getSentence().substring(endVowel));
            } else {
                sentences.get(i).setSentence(sentences.
                        get(i).getSentence().substring(0, startVowel - longestWord.length() + startFromVowel.length())
                        + longestWord + sentences.
                        get(i).getSentence().substring(endVowel - longestWord.length() + startFromVowel.length()));
            }
        }
    }

    private boolean startFromVowel(String string) {
        Pattern p = Pattern.compile("[aeyiou]");
        Matcher m = p.matcher(string);
        if (m.find()) {
            if (m.start() == 0)
                return true;
            else return false;
        }
        return false;
    }

    //6
    public void printWordInAlphabetOrder() {
        System.out.println("\nAll words in alphabet order: ");
        List<String> strings = new ArrayList<>();
        for (Sentence sentence : sentences) {
            for (Word word : sentence.getWords()) {
                strings.add(word.getWord().toLowerCase());
            }
        }
        char letter = 0;
        Collections.sort(strings);
        for (String string : strings) {
            System.out.print(string + " ");
            if (string.charAt(0) != letter) {
                System.out.println();
            }
            letter = string.charAt(0);
        }
    }

    //7
    public void sortWordAfterPercentage() {
        System.out.println("\nAll words in order of percentage vowel letters: ");
        List<String> strings = new ArrayList<>();
        for (Sentence sentence : sentences) {
            for (Word word : sentence.getWords()) {
                strings.add(word.getWord().toLowerCase());
            }
        }

        for (int i = 0; i < strings.size(); i++) {
            for (int j = 0; j < strings.size() - 1; j++) {
                if (percentageOfVowel(strings.get(j)) < percentageOfVowel(strings.get(j + 1))) {
                    String copy = strings.get(j + 1);
                    strings.set(j + 1, strings.get(j));
                    strings.set(j, copy);
                }
            }
        }

        for (String string : strings) {
            System.out.print(string + "\n");
        }
    }

    private double percentageOfVowel(String word) {
        Pattern p = Pattern.compile("[aeyiou]");
        Matcher m = p.matcher(word);
        double amount = 0;
        while (m.find()) {
            amount++;
        }
        return amount / word.length();
    }

    //8
    public void sortWordAfterFirstConsonant() {
        System.out.println("\nWords which start from vowel" +
                " in alphabet order by first consonant: ");
        List<String> strings = new ArrayList<>();
        for (Sentence sentence : sentences) {
            for (Word word : sentence.getWords()) {
                if (startFromVowel(word.getWord()) == true) {
                    strings.add(word.getWord().toLowerCase());
                }
            }
        }

        for (int i = 0; i < strings.size(); i++) {
            for (int j = 0; j < strings.size() - 1; j++) {
                if (getFirstConsonantLetter(strings.get(j))
                        > getFirstConsonantLetter(strings.get(j + 1))) {
                    String copy = strings.get(j + 1);
                    strings.set(j + 1, strings.get(j));
                    strings.set(j, copy);
                }
            }
        }

        for (String string : strings) {
            System.out.print(string + "\n");
        }
    }

    private char getFirstConsonantLetter(String word) {
        Pattern p = Pattern.compile("[^aeyiou]");
        Matcher m = p.matcher(word);
        while (m.find()) {
            return word.charAt(m.start());
        }
        return 'b';
    }

    //9
    public void sortWordAfterInputLetter() {
        System.out.println("\nSorted word after" +
                " your letter:\n Input your letter :");
        String letter = in.nextLine();
        List<String> strings = new ArrayList<>();
        for (Sentence sentence : sentences) {
            for (Word word : sentence.getWords()) {
                strings.add(word.getWord().toLowerCase());
            }
        }

        for (int i = 0; i < strings.size() * 4; i++) {
            for (int j = 0; j < strings.size() - 1; j++) {
                if (amountOfLetters(strings.get(j), letter)
                        == amountOfLetters(strings.get(j + 1), letter)) {
                    if (strings.get(j).compareTo(strings.get(j + 1)) > 0) {
                        String copy = strings.get(j + 1);
                        strings.set(j + 1, strings.get(j));
                        strings.set(j, copy);
                    }
                } else {
                    if (amountOfLetters(strings.get(j), letter)
                            < amountOfLetters(strings.get(j + 1), letter)) {
                        String copy = strings.get(j + 1);
                        strings.set(j + 1, strings.get(j));
                        strings.set(j, copy);
                    }
                }
            }
        }

        for (String string : strings) {
            System.out.print(string + "\n");
        }
    }

    private int amountOfLetters(String word, String letter) {
        Pattern p = Pattern.compile(letter);
        Matcher m = p.matcher(word);
        int count = 0;
        while (m.find()) {
            count++;
        }
        return count;
    }

    //10
    public void findRepeatOfWords() {
        System.out.println("\nTask 10\n");
        List<String> words = new ArrayList<String>();
        words.add("the");
        words.add("a");
        for (String word : words) {
            List<String> strings = new ArrayList<>();
            for (Sentence sentence : sentences) {
                strings.add(word + " " + amountOfLetters(sentence.getSentence(), word + " ")
                        + ": " + sentence.getSentence());
            }
            Collections.sort(strings);
            for (String str : strings) {
                System.out.println(str);
            }
        }
    }

    //11
    public void deleteSubstring() {
        System.out.print("\nTask 10.\nDelete substring.\nInput first letter of substring:");
        String start = in.nextLine();
        System.out.print("\nInput the last letter:");
        String end = in.nextLine();
        for (Sentence sentence : sentences) {
            System.out.println(delete(sentence.getSentence(), start, end));
        }
    }

    private String delete(String sentence, String start, String end) {
        Pattern p = Pattern.compile(start + ".*" + end);
        Matcher m = p.matcher(sentence);
        int length = 0;
        int first = 0, last = 0;
        while (m.find()) {
            if (m.end() - m.start() > length) {
                length = m.end() - m.start();
                first = m.start();
                last = m.end();
            }
        }
        return sentence.substring(0, first + 1) + sentence.substring(last + 1);
    }

    //12
    public void deleteWords() {
        System.out.println("\nInput length of words which you want to delete : ");
        int length = in.nextInt();
        for (Sentence sentence : sentences) {
            for (Word word : sentence.getWords()) {
                if (word.getWord().length() == length && startFromVowel(word.getWord())) {
                    sentence.setSentence(sentence.getSentence().replace(word.getWord(), ""));
                }
            }
            System.out.print("\n" + sentence.getSentence());
        }
    }

    //13

    //14
    public void findPolydrome() {
        System.out.println("Input number of line:");
        int numberOfLine = in.nextInt();
        StringBuilder string = new StringBuilder(sentences.get(numberOfLine).getSentence());
        int max = 0;
        int start = 0, end = 0;
        for (int i = 0; i < string.length(); i++) {
            for (int j = i + 1; j < string.length(); j++) {
                if (string.substring(i, j).equals(
                        new StringBuilder(string).reverse())
                        && string.substring(i, j).length() > max) {

                    max = string.substring(i, j).length();
                    start = i;
                    end = j;

                }
            }
        }
        System.out.println(string.substring(start, end));
    }
}
