package com.kostrych;
@FunctionalInterface
public interface Command {
    void call();
}
