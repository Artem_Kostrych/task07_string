package com.kostrych;


import java.util.*;

public class Menu {
    private static Scanner input = new Scanner(System.in);
    Locale locale;
    ResourceBundle bundle;
    private Map<String, String> menu;
    private Map<String, Command> methodsMenu;
    private Text text = new Text();

    public Menu() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();

//        menu.put("1", "1-Show task 1");
//        menu.put("2", "2-Show task 2");
//        menu.put("3", "3-Show task 3");
//        menu.put("4", "4-Show task 4");
//        menu.put("5", "5-Show task 5");
//        menu.put("6", "6-Show task 6");
//        menu.put("7", "7-Show task 7");
//        menu.put("8", "8-Show task 8");
//        menu.put("9", "9-Show task 9");
//        menu.put("10", "10-Show task 10");
//        menu.put("11", "11-Show task 11");
//        menu.put("12", "12-Show task 12");
//        menu.put("Q", "Q-Exit");

        methodsMenu.put("1", this::taskOne);
        methodsMenu.put("2", this::taskTwo);
        methodsMenu.put("3", this::taskThree);
        methodsMenu.put("4", this::taskFour);
        methodsMenu.put("5", this::taskFive);
        methodsMenu.put("6", this::taskSix);
        methodsMenu.put("7", this::taskSeven);
        methodsMenu.put("8", this::taskEight);
        methodsMenu.put("9", this::taskNine);
        methodsMenu.put("10", this::taskTen);
        methodsMenu.put("11", this::taskEleven);
        methodsMenu.put("12", this::taskTwelve);
        methodsMenu.put("13", this::internationalizeMenuUkrainian);
        methodsMenu.put("14", this::internationalizeMenuEnglish);
    }

    private void internationalizeMenuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("Q", bundle.getString("Q"));
    }

    public void taskOne() {
        text.findMaxAmount();
    }

    public void taskTwo() {
        text.outputSentencesInOrder();
    }

    public void taskThree() {
        text.findWordInFirstSentence();
    }

    public void taskFour() {
        text.findWordsInQuestions();
    }

    public void taskFive() {
        text.exchangeWords();
    }

    public void taskSix() {
        text.printWordInAlphabetOrder();
    }

    public void taskSeven() {
        text.sortWordAfterPercentage();
    }

    public void taskEight() {
        text.sortWordAfterFirstConsonant();
    }

    public void taskNine() {
        text.sortWordAfterInputLetter();
    }

    public void taskTen() {
        text.findRepeatOfWords();
    }

    public void taskEleven() {
        text.deleteSubstring();
    }

    public void taskTwelve() {
        text.deleteWords();
    }


    void outputMenu() {
        System.out.println("\nMenu");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            System.out.print((char) 27 + "[31m");
            outputMenu();
            System.out.print((char) 27 + "[34m");
            System.out.println("\nPlease, make your choice: ");
            System.out.print((char) 27 + "[33m");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).call();
            } catch (Exception e) {
                System.out.println("Exceptions");
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}
